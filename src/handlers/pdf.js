
import log from '../utils/log'
import pdf, { makePrintOptions } from '../chrome/pdf'
import { Buffer } from 'buffer';

export default async function handler(event, context, callback) {
  let rawBody = new Buffer(event.body, 'base64').toString('ascii');
  let body = JSON.parse(rawBody);

  const url = body.url;
  const base64Html = body.html;
  const printOptions = makePrintOptions({})
  let data

  log('Processing PDFification for', url, printOptions)

  const startTime = Date.now()

  try {
    if (url) {
      data = await pdf(url, printOptions)
    } else if (base64Html) {
      let html = new Buffer(base64Html).toString('ascii')
      data = await pdf(`data:text/html,${html}`, printOptions)
    }
  } catch (error) {
    console.error('Error printing pdf for', url, error)
    return callback(error)
  }

  log(`Chromium took ${Date.now() - startTime}ms to load URL and render PDF.`)

  // TODO: handle cases where the response is > 10MB
  // with saving to S3 or something since API Gateway has a body limit of 10MB
  return callback(null, {
    statusCode: 201,
    body: data,
    isBase64Encoded: true,
    headers: {
      'Content-Type': 'application/pdf',
    },
  })
}
